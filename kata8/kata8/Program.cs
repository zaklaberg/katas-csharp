﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace kata8
{
    public class Program
    {
        static void Main(string[] args) { }

        public static int SumDigProd(params int[] nums)
        {
            if (nums.Length == 1 && nums[0] < 10)
                return nums[0];

            var sumDigits = nums.Sum().ToString().ToList();
            int product = 1;

            sumDigits.ForEach(e => product *= int.Parse(e.ToString()));
            return SumDigProd(product);
        }
    }
}
