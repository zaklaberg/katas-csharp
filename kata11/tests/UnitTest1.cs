using System;
using Xunit;
using kata11;

namespace tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(8 + (8 * 2 * 0.95), new int[] { 0, 0, 1 })]
        [InlineData(2 * (8 * 2 * 0.95), new int[] { 0, 0, 1, 1 })]
        [InlineData((8 * 4 * 0.8) + (8 * 2 * 0.95), new int[] { 0, 0, 1, 2, 2, 3 })]
        public void MultipleBookDiscounts(double cost, int[] books)
        {
            Assert.Equal(cost, Books.Discount(books));
        }

        [Theory]
        [InlineData(0, new int[] { })]
        [InlineData(8, new int[] { 1 })]
        [InlineData(8, new int[] { 2 })]
        [InlineData(8, new int[] { 3 })]
        [InlineData(8*3, new int[] { 1, 1, 1 })]
        public void BasicTests(double cost, int[] books)
        {
            Assert.Equal(cost, Books.Discount(books));
        }

        [Theory]
        [InlineData(8*2*0.95, new int[] { 0, 1 })]
        [InlineData(8*3*0.9, new int[] { 0, 2, 3 })]
        [InlineData(8*4*0.8, new int[] { 0, 1, 2, 3 })]
        public void DiscountTests(double cost, int[] books)
        {
            Assert.Equal(cost, Books.Discount(books));
        }
    }
}
