﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace kata_5
{
    class Program
    {
        static void Main(string[] args)
        {
        }
    }

    public class Letters
    {
        public static char[] RemoveLetters(char[] lettersIn, string word)
        {
            var letters = new List<char>(lettersIn);
            foreach(char letter in word)
            {
                var idx = letters.FindIndex(let => let == letter);
                if (idx == -1)
                    continue;

                letters.RemoveAt(idx);
            }

            return letters.ToArray();
        }
    }
}
