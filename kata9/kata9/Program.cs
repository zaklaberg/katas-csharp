﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace kata9
{
    class Program
    {
        static void Main(string[] args) {
            Func<string, string> cap = word => char.ToUpper(word[0]) + word.Substring(1);

            string t = "I like to eat honey waffles.";
            var words = t.Split().ToList();
            words = words.Select(cap).ToList();
            words.ForEach(w => Console.WriteLine(w));

        }
    }

    public class Glory
    {
        public static string WordToPig(string word)
        {
            word = word.Trim();

            if (word.Length == 0 || (word.Length == 1 && char.IsPunctuation(word[0])))
                return word;

                if (IsVowel(word[0]))
                return word + "yay";

            bool isCapitalized = char.IsUpper(word[0]);
            word = word.ToLower();
            int vowelIndex = word.TakeWhile(c => !IsVowel(c)).Count();

            string pigword = word.Substring(vowelIndex) + word.Substring(0, vowelIndex) + "ay";
            if (isCapitalized) Capitalize(ref pigword);
            
            return pigword;
        }

        public static void Capitalize(ref string word)
        {
            word = word[0].ToString().ToUpper() + word.Substring(1);
        }

        public static string SentenceToPig(string sentence)
        {
            var words = Regex.Replace(sentence, @"[:!,.?]", " $0").Split().ToList();
            
            // below works, above doesnt work cuz join " " w/ punctuation adds extra spaces.
            // dont want to bruteforce, but.... 

            //var words = string.Join("", sentence.Where(c => !char.IsPunctuation(c))).Split().ToList();
            //var punctuation = string.Join("", sentence.Where(c => char.IsPunctuation(c)));
            var pigwords = new List<string>();
            
            words.ForEach(word => pigwords.Add(WordToPig(word).Trim()));
            return string.Join(" ", pigwords);
        }

        public static bool IsVowel(char c)
        {
            var vowels = new List<char>() { 'a', 'e', 'i', 'o', 'u' };
            return vowels.Contains(char.ToLower(c));
        }

        
    }
}
