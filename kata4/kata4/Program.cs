﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;

namespace kata4
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Enter a password...");
            var pw = Console.ReadLine();
            var strength = PasswordStrength.GetStrength(pw);
            Console.WriteLine("Your password is " + strength);
            Console.ReadKey();
        }
    }

    public class PasswordStrength
    {
        public enum StrengthCriteria
        {
            AtLeastOneLowerCaseCharacter,
            AtLeastOneUpperCaseCharacter,
            AtLeastOneNumber,
            AtLeastOneSpecialCharacter,
            AtLeastLengthEight
        }
        public static string GetStrength(string password)
        {
            List<StrengthCriteria> matches = new List<StrengthCriteria>();

            if (Regex.Matches(password, @"[a-z]").Count != 0) matches.Add(StrengthCriteria.AtLeastOneLowerCaseCharacter);
            if (Regex.Matches(password, @"[A-Z]").Count != 0) matches.Add(StrengthCriteria.AtLeastOneUpperCaseCharacter);
            if (Regex.Matches(password, @"[0-9]").Count != 0) matches.Add(StrengthCriteria.AtLeastOneNumber);
            if (Regex.Matches(password, @"[^a-zA-Z0-9 ]").Count != 0) matches.Add(StrengthCriteria.AtLeastOneSpecialCharacter);
            if (password.Length >= 8) matches.Add(StrengthCriteria.AtLeastLengthEight);
            bool invalid = password.Length < 6 || Regex.Matches(password, @"\s").Count != 0;
            
            string strength = "";
            if (invalid) strength = "invalid";
            else if (matches.Count <= 2) strength = "weak";
            else if (matches.Count <= 4) strength = "moderate";
            else if (matches.Count <= 5) strength = "strong";
            

            if (matches.Count != 5)
            {
                var allCriteria = (StrengthCriteria[])Enum.GetValues(typeof(StrengthCriteria));
                Console.WriteLine($"Your password is {strength}. To make it better, consider: ");
                foreach (var crit in allCriteria)
                {
                    if (matches.Contains(crit)) continue;
                    switch (crit)
                    {
                        case StrengthCriteria.AtLeastOneLowerCaseCharacter:
                            Console.Write("- Adding a lowercase letter.");
                            break;
                        case StrengthCriteria.AtLeastOneUpperCaseCharacter:
                            Console.WriteLine("- Adding an uppercase letter.");
                            break;
                        case StrengthCriteria.AtLeastOneNumber:
                            Console.WriteLine("- Adding a number.");
                            break;
                        case StrengthCriteria.AtLeastLengthEight:
                            Console.WriteLine("- Having at least 8 characters.");
                            break;
                        case StrengthCriteria.AtLeastOneSpecialCharacter:
                            Console.WriteLine("- Adding a special character.");
                            break;
                        default:
                            Console.WriteLine("Actually, it looks good... my bad.");
                            break;
                    }
                }
            }

            return strength;
        }
    }
}
