﻿using System;
using System.Collections.Generic;

namespace kata15
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(RomanToDecimal("MCMXLIV"));
            Console.WriteLine(RomanToDecimal("MMVI"));
        }

        public static int RomanToDecimal(string roman)
        {
            Dictionary<char, int> rtd = new Dictionary<char, int>()
            {
                { 'M', 1000 },
                { 'D', 500 },
                { 'C', 100},
                { 'L', 50 },
                { 'X', 10 },
                { 'V', 5 },
                { 'I', 1}
            };

            int sum = 0;
            for(int i = 0; i < roman.Length; ++i)
            {
                if (i + 1 < roman.Length && rtd[roman[i]] < rtd[roman[i + 1]])
                {
                    sum -= rtd[roman[i]];
                }
                else sum += rtd[roman[i]];
            }

            return sum;
        }
    }
}
