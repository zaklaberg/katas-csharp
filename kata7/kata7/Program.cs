﻿using System;
using System.Collections.Generic;

namespace kata7
{

    class Program
    {
        static void Main(string[] args)
        {
            Ironman imEvent = new Ironman();

            var competeInTriathlon = new IronmanEvent.CompeteInTri();
            var competeInRun = new IronmanEvent.CompeteInRun();
            var dontCompete = new IronmanEvent.DontCompete();

            imEvent.RegisterAttendee(new Attendee(dontCompete, "Sir Slobberfat the Third", AttendeeType.ATHLETE));
            imEvent.RegisterAttendee(new Attendee(competeInTriathlon, "(nearly) Headless Nick", AttendeeType.SPECTATOR));
            imEvent.RegisterAttendee(new Attendee(competeInRun, "Harald, King of Norway", AttendeeType.MARSHAL));

            imEvent.RollCall();
            imEvent.ConductEvent();
        }
    }
}
