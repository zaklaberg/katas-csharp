﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace kata11
{
    class Program
    {
        static void Main(string[] args) { }
    }

    public class Books
    {
        public static double Discount(params int[] books_)
        {
            // Memo
            Dictionary<int, int> books = new Dictionary<int, int>();
            foreach (int book in books_)
            {
                if (!books.ContainsKey(book))
                    books[book] = 0;
                books[book]++;
            }

            const int pricePerBook = 8;
            double cost = 0;
            
            // Iteratively remove each largest unique set
            while(books.Count != 0)
            {
                // Price of largest unique set
                double discount = books.Count switch
                {
                    2 => 0.95,
                    3 => 0.9,
                    4 => 0.8,
                    _ => 1
                };
                cost += discount * books.Count * pricePerBook;

                // Remove largest unique set
                List<int> booksToRemove = new List<int>();
                foreach (var bookKey in books.Keys.ToList())
                {
                    if (--books[bookKey] == 0)
                        booksToRemove.Add(bookKey);
                }

                booksToRemove.ForEach(bookKey => books.Remove(bookKey));
            }

            return cost;

        }
    }
}
