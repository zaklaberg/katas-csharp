﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace kata10
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(Lawyer.ToCamelCase("hello_edabit"));
            Console.WriteLine(Lawyer.ToSnakeCase("getColor"));
        }
    }

    public class Lawyer
    {
        public static string ToSnakeCase(string str)
        {
            StringBuilder sb = new StringBuilder();
            foreach(char c in str)
            {
                if (char.IsUpper(c))
                {
                    sb.Append('_');
                    sb.Append(char.ToLower(c));
                }
                else sb.Append(c);
            }

            return sb.ToString();
        }
        public static string ToCamelCase(string str)
        {
            var words = str.Split('_').ToList();
            var capitalizedWords = words.Select(Capitalize).Skip(1).ToList();
            capitalizedWords.Insert(0, words[0]);

            return string.Join("", capitalizedWords);
        }

        public static string Capitalize(string word)
        {
            return char.ToUpper(word[0]) + word.Substring(1);
        }
    }
}
