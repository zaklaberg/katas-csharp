﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kata7.IronmanEvent
{
    public class CompeteInTri : ICompeteBehaviour
    {
        public void Compete(string name, List<string> results)
        {
            results.Add($"{name} is competing in the triathlon.");
        }
    }

    public class CompeteInRun : ICompeteBehaviour
    {
        public void Compete(string name, List<string> results)
        {
            results.Add($"{name} is competing in the run.");
        }
    }

    public class DontCompete : ICompeteBehaviour
    {
        public void Compete(string name, List<string> results) { }
    }
}
