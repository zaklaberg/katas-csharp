﻿using System;
using System.Collections.Generic;

namespace Kata_2
{
    class Program
    {
        static int NumberOfFriday13thsInYear(int year)
        {
            int numThirteens = 0;
            for(int i=1;i<13;++i)
            {
                DateTime date = new DateTime(year, i, 13);
                if (date.DayOfWeek == DayOfWeek.Friday) ++numThirteens;
            }

            return numThirteens;
        }

        static List<(int, int)> YearsWithNumFriday13ths(int maxYear)
        {
            List<(int, int)> thirteens = new List<(int, int)>();
            for (int i = DateTime.Now.Year; i < maxYear; ++i)
            {
                thirteens.Add((NumberOfFriday13thsInYear(i), i));
            }
            var comparer = Comparer<(int, int)>.Create((i1, i2) => i1.Item1.CompareTo(i2.Item1));

            thirteens.Sort(comparer);
            thirteens.Reverse();

            return thirteens;
        }

        static void Main(string[] args)
        {
            Console.WriteLine(NumberOfFriday13thsInYear(2021));
            var thirteens = YearsWithNumFriday13ths(3000);
            Console.WriteLine("Number of years with 3 friday the 13ths: ");
            thirteens.ForEach(tupl =>
            {
                if (tupl.Item1 != 3) return;
                Console.WriteLine(tupl.Item2);
            });
        }
    }
}
