﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kata7
{
    public enum AttendeeType
    {
        MARSHAL,
        ATHLETE,
        SPECTATOR
    }
    public class Attendee
    {
        public AttendeeType Type { get; set; }
        public string Name { get; set; }
        private ICompeteBehaviour competeBehaviour = null;

        public Attendee(ICompeteBehaviour competeBehaviour, string name, AttendeeType type)
        {
            this.competeBehaviour = competeBehaviour;
            Name = name;
            Type = type;
        }

        public void Compete(List<string> results)
        {
            competeBehaviour.Compete(Name, results);
        }

        public string Render()
        {
            Func<char, bool> isVowel = c => { return "aeiouAEIOU".IndexOf(c) >= 0; };

            string type = Type.ToString("F");
            if (isVowel(type[0])) type = $"an {type}";
            else type = $"a {type}";
            return $"Oi, I'm {type} and my name is {Name}.";
        }
    }
}
