using System;
using Xunit;

namespace tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(0, "00:00:00")]
        [InlineData(5, "00:00:05")]
        [InlineData(60, "00:01:00")]
        [InlineData(86399, "23:59:59")]
        [InlineData(359999, "99:59:59")]
        public void TestTiems(int tiem, string format)
        {
            Assert.Equal(format, kata12.TimeFormat.GetReadableTime(tiem));
        }
    }
}
