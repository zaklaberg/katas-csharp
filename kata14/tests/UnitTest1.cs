using System;
using Xunit;
using kata14;

namespace tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("butl", "beautiful", true)]
        [InlineData("butlz", "beautiful", false)]
        [InlineData("tulb", "beautiful", false)]
        [InlineData("bbutl", "beautiful", false)]
        public void TestCanComplete(string partial, string complete, bool canComplete)
        {
            bool canCompleteActual = Program.CanComplete(partial, complete);

            Assert.Equal(canComplete, canCompleteActual);
        }
    }
}
