using System;
using Xunit;

namespace UnitTests
{
    public class PasswordTests
    {
        [Theory]
        [InlineData("stonk", "invalid")]
        [InlineData("pass word", "invalid")]
        [InlineData("password", "weak")]
        [InlineData("11081992", "weak")]
        [InlineData("mySecurePass123", "moderate")]
        [InlineData("!@!pass1", "moderate")]
        [InlineData("@S3cur1ty", "strong")]
        public void TestPasswordStrength(string password, string expectedStrength)
        {
            string actualStrength = kata4.PasswordStrength.GetStrength(password);

            Assert.Equal(expectedStrength, actualStrength);
        }
    }
}
