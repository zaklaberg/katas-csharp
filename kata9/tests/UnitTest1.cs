using System;
using Xunit;

namespace tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("have", "avehay")]
        [InlineData("cram", "amcray")]
        [InlineData("take", "aketay")]
        [InlineData("cat", "atcay")]
        [InlineData("shrimp", "impshray")]
        [InlineData("trebuchet", "ebuchettray")]
        [InlineData("ate", "ateyay")]
        [InlineData("apple", "appleyay")]
        [InlineData("oaken", "oakenyay")]
        [InlineData("eagle", "eagleyay")]
        [InlineData("flag", "agflay")]
        [InlineData("Apple", "Appleyay")]
        [InlineData("button", "uttonbay")]
        [InlineData("", "")]
        public void TestPigWords(string word, string expected)
        {
            string actual = kata9.Glory.WordToPig(word);

            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData("I like to eat honey waffles.", "Iyay ikelay otay eatyay oneyhay afflesway.")]
        [InlineData("Do you think it is going to rain today?", "Oday ouyay inkthay ityay isyay oinggay otay ainray odaytay?")]
        public void TestPigSentences(string sentence, string expected)
        {
            string actual = kata9.Glory.SentenceToPig(sentence);

            Assert.Equal(expected, actual);
        }
    }
}
