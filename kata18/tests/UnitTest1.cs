using System;
using Xunit;

namespace tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("Sshe ssselects to eat that apple. ", true)]
        [InlineData("She ssselects to eat that apple. ", false)]
        [InlineData("Beatrice samples lemonade2", false)]
        [InlineData("You ssseldom ssssspeak sso boldly, ssso messmerizingly.", true)]
        [InlineData("Steve likes to eat pancakes", false)]
        [InlineData("Sssteve likess to eat pancakess", true)]
        [InlineData("Beatrice enjoysss lemonade", true)]
        public void Tessssts(string str, bool expected)
        {
            bool actual = kata18.Program.IsParselTongue(str);

            Assert.Equal(expected, actual);
        }
    }
}
