﻿using System;
using System.Linq;

namespace kata14
{
    public class Program
    {
        static void Main(string[] args)
        {
            CanComplete("butl", "beautiful");
        }

        public static bool CanComplete(string partial, string complete)
        {
            if (partial.Length > complete.Length)
                return false;

            int matches = 0;
            int j = 0;
            for(int i = 0; i < partial.Length; ++i)
            {
                while(j < complete.Length)
                {
                    if (partial[i] == complete[j])
                    {
                        ++matches;
                        ++j;
                        break;
                    }

                    ++j;
                }
            }

            return matches == partial.Length;
        }
    }
}
