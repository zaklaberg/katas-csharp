﻿using System.Linq;
using System;
using System.Text;

namespace kata16
{
    public class Program
    {
        static void Main(string[] args) { }

        public static bool ValidateCard(long card_)
        {
            string card = card_.ToString();

            if (card.Length < 14 || card.Length > 19)
                return false;

            var sum = new StringBuilder("");
            int checkDigit = int.Parse(card.Substring(card.Length - 1));
            card = Reverse(card.Substring(0, card.Length - 1));

            for(int i = 0; i < card.Length; i++)
            {
                string outDigit = "";
                if (i % 2 != 0)
                    outDigit = card[i].ToString();
                else
                {
                    var tmpDigit = 2 * int.Parse(card[i].ToString());
                    if (tmpDigit > 9)
                        outDigit = AddDigits(tmpDigit).ToString();
                    else
                        outDigit = tmpDigit.ToString();
                }
                sum.Append(outDigit);
            }
            
            int sumDigits = AddDigits(long.Parse(sum.ToString()));
            int lastDigit = sumDigits % 10;

            if (10 - lastDigit != checkDigit)
                return false;
            return true;
        }

        public static string Reverse(string s)
        {
            var charArr = s.ToCharArray();
            Array.Reverse(charArr);
            return new string(charArr);
        }

        public static int AddDigits(long digits_)
        {
            int sum = 0;
            var digits = digits_.ToString();
            foreach (var digit in digits) {
                sum += int.Parse(digit.ToString());
            }

            return sum;
        }
    }
}
