﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Kata_1
{
    class Program
    {
        static void Main(string[] args)
        {
            string toPas = "The qUick! bRoWn fox   jumped, OVER the    lazy. dog";
            Console.WriteLine(ToPascal(toPas));
        }
        
        static string ToPascal(String str)
        {
            // Handle irregular capitalization
            string[] words = str.ToLower().Split(' ', StringSplitOptions.RemoveEmptyEntries);

            for(int i=0; i<words.Length; ++i)
            {
                // Remove punctuation
                words[i] = Regex.Replace(words[i], @"[^\w\s]", "");
                
                // Capitalize 
                words[i] = Capitalize(words[i]);
            }

            return String.Join("", words);
        }

        static string Capitalize(String str)
        {
            str = str.Trim();
            return str.Substring(0, 1).ToUpper() + str.Substring(1);
        }
    }
}
