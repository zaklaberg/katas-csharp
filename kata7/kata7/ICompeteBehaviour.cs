﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kata7
{
    public interface ICompeteBehaviour
    {
        public void Compete(string name, List<string> results);
    }
}
