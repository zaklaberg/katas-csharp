﻿using System;
using System.Collections.Generic;
using System.Text;

namespace kata7
{
    public abstract class Event
    {
        public string Name { get; set; }
        public List<Attendee> Attendees = new List<Attendee>();

        public void RegisterAttendee(Attendee atd)
        {
            Attendees.Add(atd);
        }

        public void RollCall()
        {
            Console.WriteLine($"-- ATTENDEES OF GLORIOUS EVENT {Name} --");
            Attendees.ForEach(atd => Console.WriteLine(atd.Render()));
        }

        public void ConductEvent()
        {
            List<string> results = new List<string>();
            Attendees.ForEach(atd => atd.Compete(results));

            Console.WriteLine($"\n-- RESULTS OF GLORIOUS EVENT {Name} --");
            results.ForEach(res => Console.WriteLine(res));
        }
    }


}
