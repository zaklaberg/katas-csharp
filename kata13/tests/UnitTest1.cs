using System;
using Xunit;
using kata13;

namespace tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("rgb(0, 0  ,0)", true)]
        [InlineData("rgb(255,255   ,255)", true)]
        [InlineData("rgba(0 ,0, 0,0)", true)]
        [InlineData("rgba(0,0,0,1)", true)]
        public void TestIntegerRGB(string rgb, bool isValid)
        {
            bool actual = ColourUtil.ValidRgbColour(rgb);

            Assert.Equal(isValid, actual);
        }

        [Theory]
        [InlineData("rgba(0,0,0,0.123456789)", true)]
        [InlineData("rgba(0,0,0,.8)", true)]
        [InlineData("rgba(   0 , 127,    255 , 0.1)", true)]
        [InlineData("rgb(0%,50%,100%)", true)]
        public void TestDecimalRGB(string rgb, bool isValid)
        {
            bool actual = ColourUtil.ValidRgbColour(rgb);
            Assert.Equal(isValid, actual);
        }

        [Theory]
        [InlineData("rgb(0,,0)", false)]
        [InlineData("rgb (0,0,0)", false)]
        [InlineData("rgb(0,0,0,0)", false)]
        [InlineData("rgba(0,0,0)", false)]
        [InlineData("rgb(-1,0,0)", false)]
        [InlineData("rgb(255,255,256)", false)]
        [InlineData("rgb(100%,100%,101%)", false)]
        [InlineData("rgba(0,0,0,-1)", false)]
        [InlineData("rgba(0,0,0,1.1", false)]
        public void TestOtherStuff(string rgb, bool isValid)
        {
            bool actual = ColourUtil.ValidRgbColour(rgb);
            Assert.Equal(isValid, actual);
        }
    }
}
