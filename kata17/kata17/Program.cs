﻿using System;

namespace kata17
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
        }

        public static bool ValidName(string name)
        {
            var terms = name.Split(' ');

            // Validate length
            if (terms.Length < 2 || terms.Length > 3)
                return false;

            // Last term must be a word
            if (terms[terms.Length - 1].Length < 3)
                return false;

            // If there are three terms, any term may be a word, but you cannot have initial - word - word
            if (terms.Length == 3 && terms[0].Length == 2 && terms[1].Length != 2)
                return false;

            foreach(var term in terms)
            {
                // First character must be uppercase
                if (!char.IsUpper(term[0]))
                    return false;

                // Initial
                if (term.Length == 2)
                {
                    // Must end with dot
                    if (term[1] != '.')
                        return false;
                    
                }
                // Initials should end with a dot, so 1-length terms are not valid
                else if (term.Length > 2)
                {
                    // Words cannot end with dots
                    if (term[term.Length - 1] == '.')
                        return false;
                }
                else
                    return false;
            }

            return true;
        }
    }
}
