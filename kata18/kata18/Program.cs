﻿using System;
using System.Text.RegularExpressions;
using System.Linq;

namespace kata18
{
    public class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(IsParselTongue("Sshe ssselects to eat that apple. "));
        }

        public static bool IsParselTongue(string str)
        {
            var words = str.Split(' ', StringSplitOptions.RemoveEmptyEntries);

            return words.All(IsParselWord);
        }

        public static bool IsParselWord(string str)
        {
            int lonelyS = 0;
            int multiS = 0;
            for(int i = 0; i < str.Length; ++i)
            {
                if (char.ToLower(str[i]) != 's')
                    continue;

                char prev = i - 1 < 0 ? 'k' : char.ToLower(str[i - 1]);
                char next = i + 1 > str.Length - 1 ? 'k' : char.ToLower(str[i + 1]);

                if (prev != 's' && next != 's')
                    lonelyS++;
                else
                    multiS++;
            }

            return lonelyS == 0 || multiS > 0;
        }
    }
}
