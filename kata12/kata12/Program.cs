﻿using System;

namespace kata12
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(TimeFormat.GetReadableTime(60));
        }
    }

    public class TimeFormat
    {
        public static string GetReadableTime(int secs)
        {
            int hrs = secs / (60 * 60);
            secs = secs % (60 * 60);

            int mins = secs / 60;
            secs = secs % 60;

            return $"{hrs}".PadLeft(2, '0') + ":" + $"{mins}".PadLeft(2, '0') + ":" + $"{secs}".PadLeft(2, '0');
        }
    }
}
