using System;
using System.Linq;
using Xunit;

namespace tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData(0, 0)]
        [InlineData(9, 9)]
        [InlineData(9, 8, 7)]
        [InlineData(16, 28, 6)]
        [InlineData(111111111, 1)]
        [InlineData(1, 2, 3, 4, 5, 6, 2)]
        [InlineData(8, 16, 89, 3, 6)]
        [InlineData(26, 497, 62, 841, 6)]
        [InlineData(17737, 98723, 2, 6)]
        [InlineData(123, -99, 8)]
        [InlineData(167, 167, 167, 167, 167, 3, 8)]
        [InlineData(98526, 54, 863, 156489, 45, 6156, 2)]

        public void TestSumDigProd(params int[] data)
        {
            int expected = data[data.Length - 1];

            int actual = kata8.Program.SumDigProd(data.SkipLast(1).ToArray());

            Assert.Equal(expected, actual);
        }
    }
}
