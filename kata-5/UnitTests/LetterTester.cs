using System;
using Xunit;

namespace UnitTests
{
    public class LetterTester
    {
        [Fact]
        public void TestScrambled()
        {
            char[] letters = "anryow".ToCharArray();
            char[] actual = kata_5.Letters.RemoveLetters(letters, "norway");
            char[] expected = new char[] { };

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestOneIntersect()
        {
            char[] letters = "stringw".ToCharArray();
            char[] actual = kata_5.Letters.RemoveLetters(letters, "string");
            char[] expected = new char[] { 'w' };

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestMultiIntersect()
        {
            char[] letters = "bbllgnoaw".ToCharArray();
            char[] actual = kata_5.Letters.RemoveLetters(letters, "balloon");
            char[] expected = new char[] { 'b', 'g', 'w' };

            Assert.Equal(expected, actual);
        }

        [Fact]
        public void TestMultiIntersect2()
        {
            char[] letters = "ttestu".ToCharArray();
            char[] actual = kata_5.Letters.RemoveLetters(letters, "testing");
            char[] expected = new char[] { 't', 'u' };

            Assert.Equal(expected, actual);
        }
    }
}
