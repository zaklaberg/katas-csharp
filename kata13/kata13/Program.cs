﻿using System;
using System.Text.RegularExpressions;
using System.Linq;

namespace kata13
{
    class Program
    {
        static void Main(string[] args)
        {
            ColourUtil.ValidRgbColour("rgb(0, 0  ,0)");
            //ColourUtil.ValidRgbColour("rgba(0 ,0, 0,0");
            //ColourUtil.ValidRgbColour("rgba(0,0,0)");
            //ColourUtil.ValidRgbColour("rgb( 50%, 100%, 10%)");
        }
    }

    public class ColourUtil
    {
        public static bool ValidRgbColour(string colour)
        {
            var match = Regex.Match(colour, @"rgb(a)?\((?:\s*(\d{1,3})\s*%?\s*,?){3}(\s*[0-9]*(?:\.[0-9]*)?)?\)");

            var rgb = match.Groups[2].Captures.ToList().Select(e => int.Parse(e.ToString())).ToArray();
            var isRGBOnly = match.Groups[1].Captures.Count == 0 || match.Groups[1].Captures[0].Value == "";
            var isPercent = colour.Contains("%");

            // Must have 3 rgb numbers
            if (rgb.Length != 3)
                return false;

            // Make sure RGB doesnt get an alpha, and RGBA gets an alpha
            if (isRGBOnly && match.Groups[3].Captures.Count != 0)
            {
                if (match.Groups[3].Captures[0].Value != "")
                    return false;
            }
            if (!isRGBOnly)
            {
                if (match.Groups[3].Captures.Count == 0)
                    return false;
                if (match.Groups[3].Captures[0].Value == "")
                    return false;
            }

            // Validate rgb numbers
            for (int i = 0; i < rgb.Length; ++i)
            {
                // Never less than zero
                if (rgb[i] < 0)
                    return false;

                // Never greater than 255
                if (rgb[i] > 255)
                    return false;
                // If percent, never greater than 100
                if (isPercent && rgb[i] > 100)
                    return false;
            }

            // Validate alpha
            if (!isRGBOnly)
            {
                var alpha = decimal.Parse(match.Groups[3].Captures[0].Value);
                if (alpha > 1)
                    return false;
                if (alpha < 0)
                    return false;
            }

            return true;
        }
    }
}
