using System;
using Xunit;
using kata17;

namespace tests
{
    public class UnitTest1
    {
        [Theory]
        [InlineData("H. Wells", true)]
        [InlineData("H. G. Wells", true)]
        [InlineData("Herbert G. Wells", true)]
        [InlineData("Herbert", false)]
        [InlineData("h. Wells", false)]
        [InlineData("H Well", false)]
        [InlineData("H. George Wells", false)]
        [InlineData("H. George W.", false)]
        [InlineData("Herb. George Wells", false)]
        public void TestValidNames(string name, bool expected)
        {
            bool actual = Program.ValidName(name);

            Assert.Equal(expected, actual);
        }
    }
}
